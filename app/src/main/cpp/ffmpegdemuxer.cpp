#include <jni.h>
#include <string>
extern "C" {
#include <libavformat/avformat.h>
}

AVFormatContext* _ctx;
AVPacket _pkt;

extern "C"
JNIEXPORT jint JNICALL
Java_cyc_rtsph264player_FFmpegDemuxer_FFmpegOpen(JNIEnv *env, jobject instance, jstring url_) {
    const char *url = env->GetStringUTFChars(url_, 0);

    av_register_all();
    AVDictionary* dict = NULL;
    av_dict_set(&dict, "rtsp_transport", "tcp", 0);
    if (avformat_open_input(&_ctx, url, NULL, &dict) != 0) {
        return -1;
    }
    if (avformat_find_stream_info(_ctx, NULL) != 0) {
        return -1;
    }

    env->ReleaseStringUTFChars(url_, url);
    return 0;
}

extern "C"
JNIEXPORT void JNICALL
Java_cyc_rtsph264player_FFmpegDemuxer_FFmpegClose(JNIEnv *env, jobject instance) {

    if (_ctx != NULL) {
        avformat_close_input(&_ctx);
    }

}

extern "C"
JNIEXPORT jbyteArray JNICALL
Java_cyc_rtsph264player_FFmpegDemuxer_FFmpegFrame(JNIEnv *env, jobject instance) {
    jbyteArray jbArray = NULL;

    av_init_packet(&_pkt);
    if (av_read_frame(_ctx, &_pkt) == 0) {
        jbArray = env->NewByteArray(_pkt.size);
        env->SetByteArrayRegion(jbArray, 0, _pkt.size, (const jbyte*)_pkt.data);
        av_packet_unref(&_pkt);
    }

    return jbArray;
}