package cyc.rtsph264player;

import android.animation.TimeAnimator;
import android.content.SharedPreferences;
import android.media.MediaCodec;
import android.media.MediaFormat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import java.io.IOException;
import java.nio.ByteBuffer;

public class MainActivity extends AppCompatActivity {

    static final int FPS = 24;

    SurfaceView surfaceView;
    LinearLayout layout;
    EditText editText;
    Button button;

    FFmpegDemuxer mDemuxer;
    MediaCodecWrapper mCodec;
    TimeAnimator mTimeAnimator = new TimeAnimator();
    int frameCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        surfaceView = (SurfaceView)findViewById(R.id.surfaceView);
        layout = (LinearLayout)findViewById(R.id.controlLayout);
        editText = (EditText)findViewById(R.id.editText);
        button = (Button)findViewById(R.id.button);

        SharedPreferences preferences = getSharedPreferences("simplePlayer", MODE_PRIVATE);
        editText.setText(preferences.getString("uri", "/sdcard/vid_bigbuckbunny.ts"));

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = getSharedPreferences("simplePlayer", MODE_PRIVATE).edit();
                editor.putString("uri", editText.getText().toString());
                editor.commit();

                try {
                    mCodec = MediaCodecWrapper.fromVideoFormat(MediaFormat.createVideoFormat("video/avc", 0, 0),
                            surfaceView.getHolder().getSurface());
                } catch (IOException e) {
                    e.printStackTrace();
                }

                mDemuxer = new FFmpegDemuxer();
                mDemuxer.FFmpegOpen(editText.getText().toString());

                final MediaCodec.BufferInfo out_bufferInfo = new MediaCodec.BufferInfo();
                do {
                    byte[] frame = mDemuxer.FFmpegFrame();
                    if (frame != null && frame.length > 0) {
                        try {
                            mCodec.writeSample(ByteBuffer.wrap(frame), frameCount * 1000000 / FPS, 0);
                            frameCount++;
                        } catch (MediaCodecWrapper.WriteException e) {
                            e.printStackTrace();
                        }
                    }
                } while (!mCodec.peekSample(out_bufferInfo));

                mTimeAnimator.setTimeListener(new TimeAnimator.TimeListener() {
                    @Override
                    public void onTimeUpdate(TimeAnimator animation, long totalTime, long deltaTime) {
                        if (out_bufferInfo.presentationTimeUs <= totalTime * 1000) {
                            Log.i("test", "out" + out_bufferInfo.presentationTimeUs + " total" + totalTime);
                            mCodec.popSample(true);
                            byte[] frame = mDemuxer.FFmpegFrame();
                            if (frame != null && frame.length > 0) {
                                try {
                                    mCodec.writeSample(ByteBuffer.wrap(frame), frameCount * 1000000 / FPS, 0);
                                    frameCount++;
                                    mCodec.peekSample(out_bufferInfo);
                                } catch (MediaCodecWrapper.WriteException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                mTimeAnimator.end();
                            }
                        }
                    }
                });
                mTimeAnimator.start();
                layout.setVisibility(View.INVISIBLE);
            }
        });
    }
}
