package cyc.rtsph264player;

/**
 * Created by cheny on 2016/10/16.
 */

public class FFmpegDemuxer {
    static {
        System.loadLibrary("FFmpegDemuxer");
    }
    public native int FFmpegOpen(String url);
    public native void FFmpegClose();
    public native byte[] FFmpegFrame();
}
